# README #

The project is about studying different distributed algorithms for maintaining the consistency of the shared state in a collaborative text editor.
Multiple instances can insert and delete items from the same text. In order to maintain consistency distributed algorithms like CBCAST, ABCAST, dOpt and Jupiter have to be applied.

### How do I get set up? ###
In order to run the project you have to install ant. Depending on the algorithm you want to test, there is an associated bash script that starts the instances and generates commands that fill the editor. For each algorithm except dOpt and Jupiter, there is a ``manual'' version, that lets you insert text by hand on each editor client, and the output should be the same on any instance.

By default, the number of instances that is started at a time is 3. Each client editor has a port and an IP associated, information that has to be updated in the file named ``config''.

Example for running the program:
./run_all_abcast.sh - starts 3 instances running the editor implemented using ABCAST algorithm and allows the user to do insert and delete operations from the editor by hand
./run_abcast_test.sh - runs the editor implemented with ABCAST algorithm in testing mode, the commands are received as input from a file

If more than 3 clients are required the following files have to be updated:
-> config: add a port value and an IP for the extra clients
-> build.xml: specify information about the new instances of the clients to be started (please see how the other clients are started as an example)