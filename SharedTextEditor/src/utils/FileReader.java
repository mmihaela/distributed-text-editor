package utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class FileReader {

	public static ArrayList<Instance> readConfig() {
		ArrayList<Instance> cfgInstances = new ArrayList<Instance>();
		Instance instance;
		String cfgLine;
		String[] instanceComponents;

		try {
			FileInputStream fstream = new FileInputStream(Constants.CONFIG_FILE);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader bufReader = new BufferedReader(
					new InputStreamReader(in));

			/* read configuration file line by line */
			while ((cfgLine = bufReader.readLine()) != null) {
				instanceComponents = cfgLine.split(" ");
				System.out.println(cfgLine);
				if (instanceComponents.length != 3) {
					System.err
							.println("Invalid configuration file. Program will exit.");
					System.err
							.println("Valid configuration line: <instance_name>	<hostname>	<port>");
					System.exit(1);
				}

				instance = new Instance(instanceComponents[0],
						instanceComponents[1],
						Integer.valueOf(instanceComponents[2]));

				cfgInstances.add(instance);
			}

			bufReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return cfgInstances;
	}

	public static ArrayList<Operation> readCommands() {
		ArrayList<Operation> opList = new ArrayList<Operation>();
		Operation op;
		String opLine;
		String[] operationComponents;
		Integer position;

		try {
			FileInputStream fstream = new FileInputStream(Constants.COMMAND_FILE);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader bufReader = new BufferedReader(
					new InputStreamReader(in));

			/* read configuration file line by line */
			while ((opLine = bufReader.readLine()) != null) {
				operationComponents = opLine.split("[()]");

				switch (operationComponents[0]) {
				case Constants.INS_OP:
					String[] opInfo = operationComponents[1].split(",");
					String letter = opInfo[0].substring(1, 2);
					position = Integer.valueOf(opInfo[1]); 
					
					op = new Operation(Constants.INS_OP, position, letter);
					opList.add(op);
					break;
				case Constants.DEL_OP:
					position = Integer.valueOf(operationComponents[1]);
					
					op = new Operation(Constants.DEL_OP, position, null);
					opList.add(op);

					break;
				default:
					System.err
							.println("Invalid operation! The client will exit");
					System.exit(1);
				}

			}

			bufReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return opList;
	}

	public static Instance getInstanceWithName(String id,
			ArrayList<Instance> instances) {
		for (int i = 0; i < instances.size(); ++i) {
			if (instances.get(i).getInstanceName().equals(id)) {
				return instances.get(i);
			}
		}

		return null;
	}
}
