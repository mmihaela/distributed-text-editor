package utils;

import message_types.MessageJupiter;

public class TransformJupiter {
	/**
	 * Transformation when both operation are insert operations
	 * 
	 * @param msg1
	 *            the message extracted from the receive queue
	 * @param msg2
	 *            the message extracted from the log queue
	 */
	public static Operation transformT11(MessageJupiter msg1, MessageJupiter msg2) {
		Integer posMsg1 = msg1.getOp().getPosition();
		Integer posMsg2 = msg2.getOp().getPosition();

		String charMsg1 = msg1.getOp().getCharacter();
		String charMsg2 = msg2.getOp().getCharacter();

		System.out.println("Transform t11");

		/* compare operations' positions */
		/* the operation remains unchanged */
		if (posMsg1 < posMsg2) {
			return new Operation(Constants.INS_OP, posMsg1, charMsg1);
		}

		if (posMsg1 > posMsg2) {
			return new Operation(Constants.INS_OP, posMsg1 + 1, charMsg1);
		}

		if (charMsg1.equals(charMsg2)) {
			System.out.println("Are equal");
			return null;
		}
		
		if (msg1.getProcessId() > msg2.getProcessId())
			return new Operation(Constants.INS_OP, posMsg1 + 1, charMsg1);
		
		return new Operation(Constants.INS_OP, posMsg1, charMsg1);
	}

	/**
	 * Transformation when the operation from the request queue is an insertion
	 * and the operation from the log queue is a deletion operation.
	 * 
	 * @param msgQueue
	 *            the message extracted from the receive queue
	 * @param msgLog
	 *            the message extracted from the log queue
	 */
	public static Operation transformT12(MessageJupiter msg1, MessageJupiter msg2) {
		Integer posMsg1 = msg1.getOp().getPosition();
		Integer posMsg2 = msg2.getOp().getPosition();

		String charMsg1 = msg1.getOp().getCharacter();
		
		System.out.println("Transform t12");

		if (posMsg1 <= posMsg2)
			return new Operation(Constants.INS_OP, posMsg1, charMsg1);

		if (posMsg1 > posMsg2) {
			return new Operation(Constants.INS_OP, posMsg1 - 1, charMsg1);
		}
		
		return null;
	}

	/**
	 * Transformation when the operation from the request queue is a deletion
	 * and the operation from the log queue is an insertion operation.
	 * 
	 * @param msgQueue
	 *            the message extracted from the receive queue
	 * @param msgLog
	 *            the message extracted from the log queue
	 */
	public static Operation transformT21(MessageJupiter msg1, MessageJupiter msg2) {
		Integer posMsg1 = msg1.getOp().getPosition();
		Integer posMsg2 = msg2.getOp().getPosition();

		String charMsg1 = msg1.getOp().getCharacter();
		System.out.println("Transform t21");

		if (posMsg1 <= posMsg2)
			return new Operation(Constants.DEL_OP, posMsg1, charMsg1);
		
		if (posMsg1 > posMsg2) {
			return new Operation(Constants.DEL_OP, posMsg1 + 1, charMsg1);
		}
		
		return null;
	}

	/**
	 * Transformation when both operation are deletion operations
	 * 
	 * @param msgQueue
	 *            the message extracted from the receive queue
	 * @param msgLog
	 *            the message extracted from the log queue
	 */
	public static Operation transformT22(MessageJupiter msg1, MessageJupiter msg2) {
		Integer posMsg1 = msg1.getOp().getPosition();
		Integer posMsg2 = msg2.getOp().getPosition();

		String charMsg1 = msg1.getOp().getCharacter();

		System.out.println("Transform t22");

		if (posMsg1 <= posMsg2)
			return new Operation(Constants.DEL_OP, posMsg1, charMsg1);
		
		if (posMsg1 > posMsg2) {
			return new Operation(Constants.DEL_OP, posMsg1 - 1, charMsg1);
		}
		
		return null;
	}
}
