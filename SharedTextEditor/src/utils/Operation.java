package utils;

import java.io.Serializable;

public class Operation implements Serializable{
	
	private static final long serialVersionUID = -2366938001466574475L;
	
	private String opName;
	private Integer position;
	private String character;
	
	public String getCharacter() {
		return character;
	}

	public void setCharacter(String character) {
		this.character = character;
	}

	public Operation(String opName, Integer position, String character) {
		this.setOpName(opName);
		this.setPosition(position);
		this.character = character;
	}

	public String getOpName() {
		return opName;
	}

	public void setOpName(String opName) {
		this.opName = opName;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public String toString() {
		return "[OP]: " + opName + " [POS]: " + this.position;
	}
}
