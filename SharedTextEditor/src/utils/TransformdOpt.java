package utils;

import message_types.MessagedOpt;

public class TransformdOpt {
	/**
	 * Transformation when both operation are insert operations
	 * 
	 * @param msgQueue
	 *            the message extracted from the receive queue
	 * @param msgLog
	 *            the message extracted from the log queue
	 */
	public static void transformT11(MessagedOpt msgQueue, MessagedOpt msgLog) {
		Integer posMsgQueue = msgQueue.getOp().getPosition();
		Integer posMsgLog = msgLog.getOp().getPosition();

		String charMsgQueue = msgQueue.getOp().getCharacter();
		String charMsgLog = msgLog.getOp().getCharacter();

		int comparedPrio;

		System.out.println("Transform t11");

		/* compare operations' positions */
		/* the operation remains unchanged */
		if (posMsgQueue < posMsgLog) {
			msgQueue.setTransformedOp(msgQueue.getOp());
			return;
		}

		if (posMsgQueue > posMsgLog) {
			int newPosition = msgQueue.getOp().getPosition() + 1;
			Operation newOp = new Operation(msgQueue.getOp().getOpName(), newPosition, msgQueue.getOp().getCharacter());
			msgQueue.setTransformedOp(newOp);
			return;
		}

		if (charMsgQueue.equals(charMsgLog)) {
			msgQueue.setTransformedOp(null);
			return;
		}

		/* compare priorities */
		comparedPrio = msgQueue.getPriority().comparePriorities(
				msgLog.getPriority());
		switch (comparedPrio) {
		case Constants.SMALLER:
			/* operation remains unchanged */
			return;
		case Constants.BIGGER:
			int newPosition = msgQueue.getOp().getPosition() + 1;
			Operation newOp = new Operation(msgQueue.getOp().getOpName(), newPosition, msgQueue.getOp().getCharacter());
			msgQueue.setTransformedOp(newOp);
			return;
		}
	}

	/**
	 * Transformation when the operation from the request queue is an insertion
	 * and the operation from the log queue is a deletion operation.
	 * 
	 * @param msgQueue
	 *            the message extracted from the receive queue
	 * @param msgLog
	 *            the message extracted from the log queue
	 */
	public static void transformT12(MessagedOpt msgQueue, MessagedOpt msgLog) {
		Integer posMsgQueue = msgQueue.getOp().getPosition();
		Integer posMsgLog = msgLog.getOp().getPosition();

		System.out.println("Transform t12");

		if (posMsgQueue < posMsgLog)
			return;

		if (posMsgQueue > posMsgLog) {
			int newPosition = msgQueue.getOp().getPosition() - 1;
			Operation newOp = new Operation(msgQueue.getOp().getOpName(), newPosition, msgQueue.getOp().getCharacter());
			msgQueue.setTransformedOp(newOp);
			return;
		}
	}

	/**
	 * Transformation when the operation from the request queue is a deletion
	 * and the operation from the log queue is an insertion operation.
	 * 
	 * @param msgQueue
	 *            the message extracted from the receive queue
	 * @param msgLog
	 *            the message extracted from the log queue
	 */
	public static void transformT21(MessagedOpt msgQueue, MessagedOpt msgLog) {
		Integer posMsgQueue = msgQueue.getOp().getPosition();
		Integer posMsgLog = msgLog.getOp().getPosition();

		System.out.println("Transform t21");

		if (posMsgQueue < posMsgLog)
			return;

		if (posMsgQueue > posMsgLog) {
			int newPosition = msgQueue.getOp().getPosition() + 1;
			Operation newOp = new Operation(msgQueue.getOp().getOpName(), newPosition, msgQueue.getOp().getCharacter());
			msgQueue.setTransformedOp(newOp);
			return;
		}
	}

	/**
	 * Transformation when both operation are deletion operations
	 * 
	 * @param msgQueue
	 *            the message extracted from the receive queue
	 * @param msgLog
	 *            the message extracted from the log queue
	 */
	public static void transformT22(MessagedOpt msgQueue, MessagedOpt msgLog) {
		Integer posMsgQueue = msgQueue.getOp().getPosition();
		Integer posMsgLog = msgLog.getOp().getPosition();

		System.out.println("Transform t22");

		if (posMsgQueue < posMsgLog)
			return;

		if (posMsgQueue > posMsgLog) {
			int newPosition = msgQueue.getOp().getPosition() - 1;
			Operation newOp = new Operation(msgQueue.getOp().getOpName(), newPosition, msgQueue.getOp().getCharacter());
			msgQueue.setTransformedOp(newOp);
			return;
		}

		msgQueue.setTransformedOp(null);
	}
}
