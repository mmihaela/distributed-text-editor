package utils;

import java.io.Serializable;
import java.util.ArrayList;

public class Priority implements Serializable{
	private static final long serialVersionUID = -7259656758571374175L;
	public ArrayList<Integer> priorityArray;
	
	public Priority() {
		priorityArray = new ArrayList<Integer>();
	}
	
	public void updatePrio(Priority newPrio) {
		this.priorityArray.clear();
		this.priorityArray.addAll(newPrio.priorityArray);
	}
	
	public boolean isEmpty() {
		return priorityArray.isEmpty();
	}
	
	public int comparePrioSize(Priority priority) {
		return this.priorityArray.size() - priority.priorityArray.size();
	}
	
	public void updateWithMaxPrio(Priority otherPrio) {
		for (int j = 0; j < this.priorityArray.size(); j++) {
			if (this.priorityArray.get(j).equals(otherPrio.priorityArray.get(j)))
				continue;
			if (this.priorityArray.get(j) < otherPrio.priorityArray.get(j)) {
				updatePrio(otherPrio);
				break;
			}
			break;
		}
	}
	
	public int comparePriorities(Priority otherPrio) {
		int comparedSize = comparePrioSize(otherPrio);
		
		/* if the two priorities have the same size compare their content */
		if (comparedSize != 0)
			return comparedSize;
		
		for (int i = 0; i < this.priorityArray.size(); i++) {
			if (this.priorityArray.get(i).equals(otherPrio.priorityArray.get(i)))
				continue;
			
			if (this.priorityArray.get(i) > otherPrio.priorityArray.get(i))
				return 1;

			if (this.priorityArray.get(i) < otherPrio.priorityArray.get(i))
				return -1;
		}
		
		return 0;
	}
	
	public String toString() {
		return "Priority: " + priorityArray;
	}
}
