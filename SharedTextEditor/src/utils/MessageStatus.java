package utils;

import java.io.Serializable;

public class MessageStatus implements Serializable{
	
	private static final long serialVersionUID = -923707458538075915L;
	private Integer noGeneratedMsg;
	private Integer noRecvMsg;

	public MessageStatus() {
		this.setNoGeneratedMsg(0);
		this.setNoRecvMsg(0);
	}

	public Integer getNoGeneratedMsg() {
		return noGeneratedMsg;
	}

	public void setNoGeneratedMsg(Integer noGeneratedMsg) {
		this.noGeneratedMsg = noGeneratedMsg;
	}

	public Integer getNoRecvMsg() {
		return noRecvMsg;
	}

	public void setNoRecvMsg(Integer noRecvMsg) {
		this.noRecvMsg = noRecvMsg;
	}
	
	public void incNoGeneratedMsg() {
		this.noGeneratedMsg++;
	}
	
	public void incNoRecvMsg() {
		this.noRecvMsg++;
	}
	
	public String toString() {
		return "[Generated]: " + this.noGeneratedMsg + " [Recv]: " +this.noRecvMsg;
	}
}
