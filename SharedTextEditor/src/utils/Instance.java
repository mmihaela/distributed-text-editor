package utils;

public class Instance {

	private String instanceName;
	private String hostname;
	private Integer port;
	
	public Instance(String instanceName, String hostname, Integer port) {
		this.setInstanceName(instanceName);
		this.setHostname(hostname);
		this.setPort(port);
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
	
	@Override 
	public String toString() {
		return "Instance name: " + getInstanceName() + "Hostname: " + getHostname() + " Port: " + getPort() + "\n";
	}
	
}
