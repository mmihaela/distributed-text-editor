package utils;

import java.io.Serializable;
import java.util.ArrayList;

public class AbcastStatus implements Serializable, Comparable<AbcastStatus>{
	private static final long serialVersionUID = -8916626163409812631L;
	public Long uid;
	public ArrayList<Integer> vectorTime;
	
	public AbcastStatus(Long uid, ArrayList<Integer> vt) {
		vectorTime = new ArrayList<Integer>();
		vectorTime.addAll(vt);
		this.uid = uid;
	}

	@Override
	public int compareTo(AbcastStatus o) {
		boolean areEqual = true;
		ArrayList<Integer> otherVectorTime = o.vectorTime;
		
		for (int i = 0; i < vectorTime.size(); i++) {
			if (vectorTime.get(i) > otherVectorTime.get(i))
				return 1;
			if (vectorTime.get(i) < otherVectorTime.get(i))
				areEqual = false;
		}
		
		if (areEqual)
			return 0;
		
		return -1;
	}
	
	@Override
	public String toString() {
		return "[uid]" + uid + " [vt] " + vectorTime;
	}
	
}
