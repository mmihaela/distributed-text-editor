package utils;

import communication.IAlgorithm;

public class Constants {
	
	public static final int frameWidth		= 500;
	public static final int frameHeight		= 400;
	
	public static final int textAreaRows	= 20;
	public static final int textAreaColumns	= 50;
	
	public static final String CONFIG_FILE	= "config";
	
	public static final String INS_OP		= "ins";
	public static final String DEL_OP		= "del";
	
	public static final int WAITING_TIME	= 500;
	
	//public static final String COMMAND_FILE	= "commands";
	public static final String COMMAND_FILE	= "driver.txt";
	
	/* algorithm types */
	public static final String VECTOR_TIME	= "VectorTime";
	public static final String CBCAST		= "CBCAST";
	public static final String ABCAST		= "ABCAST";
	public static final String TOTAL_ORDER	= "TotalOrder";
	public static final String THREE_PHASE	= "3Phase";
	public static final String DOPT			= "dOpt";
	public static final String JUPITER		= "jupiter";
	
	/* message types for three phase algorithms */
	public static final String REVISE_TS	= "revise_ts";
	public static final String PROPOSED_TS	= "proposed_ts";
	public static final String FINAL_TS		= "final_ts";
	
	public static final int SMALLER			= -1;
	public static final int EQUAL			= 0;
	public static final int BIGGER			= 1;
	
	public static final int ROOT_NODE		= 0;
	public static final int SERVER_NODE		= 0;
	
	public static final int TOKEN_HOLDER	= 0;
	public static final String SET_ORDER	= "set-order";
	
	public static int processId;
	
	public static String algorithm;
	
	public static IAlgorithm algorithm_obj;
	
	public static boolean runTest;

}
