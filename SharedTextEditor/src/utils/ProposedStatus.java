package utils;

public class ProposedStatus {

	public int timestamp;
	public int no_nodes;
	
	public ProposedStatus( ) {
		timestamp	= 0;
		no_nodes	= 0;
	}
	
	public ProposedStatus(int timestamp, int no_nodes) {
		this.timestamp 	= timestamp;
		this.no_nodes	= no_nodes;
	}
	
	@Override
	public String toString() {
		return "[timestamp] " + this.timestamp + " [Nodes] " + this.no_nodes;
	}
}
