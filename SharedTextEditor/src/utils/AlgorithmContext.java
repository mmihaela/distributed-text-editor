package utils;

import gui.IGUI;
import communication.ABCAST;
import communication.CBCAST;
import communication.DOpt;
import communication.IAlgorithm;
import communication.Jupiter;
import communication.ThreePhase;
import communication.TotalOrder;
import communication.VectorTime;

public class AlgorithmContext {
	private IAlgorithm strategy;
	
	public AlgorithmContext(Integer size, IGUI gui) {
		switch (Constants.algorithm) {
		case Constants.CBCAST:
			strategy = new CBCAST(size, gui);
			break;
		case Constants.ABCAST:
			strategy = new ABCAST(size, gui);
			break;
		case Constants.TOTAL_ORDER:
			strategy = new TotalOrder(gui);
			break;
		case Constants.VECTOR_TIME:
			strategy = new VectorTime(size, gui);
			break;
		case Constants.THREE_PHASE:
			strategy = new ThreePhase(size, gui);
			break;
		case Constants.DOPT:
			strategy = new DOpt(size, gui);
			break;
		case Constants.JUPITER:
			strategy = new Jupiter(size, gui);
			break;
		}
	}
	
	public IAlgorithm getStrategy() {
		return strategy;
	}
	
	
}
