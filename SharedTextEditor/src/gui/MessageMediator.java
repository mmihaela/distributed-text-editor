package gui;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import utils.FileReader;
import utils.Operation;
import communication.IAlgorithm;

public class MessageMediator {
	private ArrayList<Integer> vectorTime;
	private IAlgorithm strategy;

	public MessageMediator(Integer size, IGUI gui, IAlgorithm strategy) {
		vectorTime = new ArrayList<Integer>();

		for (int i = 0; i < size; ++i)
			vectorTime.add(0);

		this.strategy = strategy;
	}

	/**
	 * Read commands from input file and send them to the other corresponding
	 * algorithm
	 */
	public void commandReader() {
		Random rand = new Random();
		ArrayList<Operation> ops = FileReader.readCommands();

		for (Operation op : ops) {
			strategy.internalMessage(op);
			try {
				TimeUnit.MILLISECONDS.sleep(randInt(rand));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	int randInt(Random rand) {
		return rand.nextInt(401) + 100;
	}
	
	long nextLong(Random rng, long n) {
		   // error checking and 2^x checking removed for simplicity.
		   long bits, val;
		   do {
		      bits = (rng.nextLong() << 1) >>> 1;
		      val = bits % n;
		   } while (bits-val+(n-1) < 0L);
		   return val;
		}

}
