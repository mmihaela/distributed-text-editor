package gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import utils.Constants;
import utils.Operation;

public class GUI extends JPanel implements IGUI {

	private static final long serialVersionUID = 6958018024759553447L;
	protected JTextArea textArea;
	protected String initialText;
	protected boolean isInsideModification;

	public GUI() {
		super(new GridBagLayout());
		isInsideModification = false;
		JButton algorithmButton;

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((dim.width - Constants.frameWidth) / 2,
				(dim.height - Constants.frameHeight) / 2);

		textArea = new JTextArea(Constants.textAreaRows,
				Constants.textAreaColumns);
		textArea.setEditable(true);

		JScrollPane scrollPane = new JScrollPane(textArea);
		algorithmButton = new JButton(Constants.algorithm + " - "
				+ Constants.processId);

		/* Add Components to this panel. */
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = GridBagConstraints.REMAINDER;

		c.fill = GridBagConstraints.BOTH;
		add(scrollPane, c);

		c.fill = GridBagConstraints.CENTER;
		add(algorithmButton, c);

		addListeners();
	}

	private void addListeners() {
		/*
		 * find the initial string before adding a new character
		 */
		textArea.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				initialText = textArea.getText();
			}
		});

		/*
		 * Add insert and update listeners for the modification of the text area
		 */
		textArea.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent e) {
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				if (isInsideModification)
					return;

				int offset = e.getOffset();

				synchronized (textArea) {
					String currentString = textArea.getText();
					String insertedChar = currentString.substring(offset,
							offset + 1);
					Operation op = new Operation(Constants.INS_OP, offset,
							insertedChar);

					Constants.algorithm_obj.internalMessage(op);
				}
				

			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				if (isInsideModification)
					return;

				int offset = e.getOffset();
				Operation op = new Operation(Constants.DEL_OP, offset, null);
				Constants.algorithm_obj.internalMessage(op);
			}
		});
	}

	@Override
	public void insert(int position, String text) {
		synchronized (textArea) {
			String currentText = textArea.getText();

			/* add character on the last postion */
			if (position >= currentText.length()) {
				isInsideModification = true;
				currentText += text;
				textArea.setText(currentText);
				isInsideModification = false;
				return;
			}
			
			/* add character on the first position */
			if (position < 0) {
				isInsideModification = true;
				currentText = text + currentText;
				textArea.setText(currentText);
				isInsideModification = false;
				return;
			}

			String suffix = currentText.substring(position);
			String prefix = currentText.substring(0, position);

			isInsideModification = true;
			textArea.setText(prefix + text + suffix);
			isInsideModification = false;
		}
	}

	@Override
	public void delete(int position) {
		synchronized (textArea) {

			String currentText = textArea.getText();

			if (currentText.length() == 0)
				return;
			
			/* delete last character */
			if (position >= currentText.length()) {
				isInsideModification = true;
				currentText = currentText.substring(0, currentText.length() - 1);
				textArea.setText(currentText);
				System.err.println("Position exceeds the size of the text!");
				isInsideModification = false;
				return;
			}

			/* delete first character */
			if (position <= 0) {
				isInsideModification = true;
				currentText = currentText.substring(1, currentText.length());
				textArea.setText(currentText);
				System.err.println("Delete first char!");
				isInsideModification = false;
				return;
			}
			
			System.out.println("Position is: " + position);
			
			String suffix = currentText.substring(position + 1);
			String prefix = currentText.substring(0, position);

			isInsideModification = true;
			textArea.setText(prefix + suffix);
			isInsideModification = false;
		}
	}

	@Override
	public synchronized void multiplexMessage(Operation op) {
		/* extract and deliver the message */
		if (op == null) {
			System.out.println("Null message! do not deliver");
			return;
		}
			
		switch (op.getOpName()) {
		case Constants.INS_OP:
			insert(op.getPosition(), op.getCharacter());
			break;
		case Constants.DEL_OP:
			delete(op.getPosition());
			break;
		}

	}

}
