package gui;

import utils.Operation;

public interface IGUI {

	public void insert(int position, String text);
	
	public void delete(int position);
	
	public void multiplexMessage(Operation op);

}
