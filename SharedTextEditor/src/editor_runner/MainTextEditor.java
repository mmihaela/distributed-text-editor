package editor_runner;

import gui.GUI;
import gui.IGUI;
import gui.MessageMediator;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFrame;

import networking.NetworkAccepter;
import networking.NetworkConnector;
import networking.NetworkWriterSocket;
import utils.AlgorithmContext;
import utils.Constants;
import utils.FileReader;
import utils.Instance;
import communication.IAlgorithm;

public class MainTextEditor {

	private static IGUI createAndShowGUI() {
		GUI editorGUI = new GUI();

		// Create and set up the window.
		JFrame frame = new JFrame("Shared Text Editor");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Add contents to the window.
		frame.add(editorGUI);

		// Display the window.
		frame.pack();
		frame.setVisible(true);

		return editorGUI;
	}

	/**
	 * The main function should receive as argument the type of the used
	 * algorithm and the filename of the configuration file
	 * 
	 * @param args
	 *            the name of the used algorithm and the configuration filename
	 */
	public static void main(String[] args) {

		if (args.length != 4) {
			System.out.println("Incorrect number of arguments!");
			System.out
					.println("Usage: java MainTextEditor <algorithm> <config_filename> <instance_name> <with_test>");
			System.exit(1);
		}

		Constants.processId = Integer.valueOf(args[2]) - 1;
		Constants.algorithm = args[0];
		Constants.runTest = (args[3].equals("true")) ? true : false;

		/* read configuration file */
		ArrayList<Instance> instances = FileReader.readConfig();
		Instance currentInstance = FileReader.getInstanceWithName(args[2],
				instances);

		/* instantiate GUI */
		IGUI editorGUI = createAndShowGUI();

		AlgorithmContext algCtx = new AlgorithmContext(instances.size(),
				editorGUI);
		IAlgorithm strategy = algCtx.getStrategy();

		Constants.algorithm_obj = strategy;

		/* accept connections from other instances */
		NetworkAccepter networkAccepter = new NetworkAccepter(currentInstance,
				instances.size() - 1, strategy);
		networkAccepter.start();

		/* connect to other text editor instances */
		NetworkConnector networkConnector = new NetworkConnector(
				currentInstance, instances);
		networkConnector.start();

		try {
			networkConnector.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		/* Sockets connected to text editor instances */
		HashMap<Integer, NetworkWriterSocket> connectors = networkConnector
				.getConnectSockets();

		System.out.println("Set connected clients");
		strategy.setConnectedClients(connectors);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		MessageMediator mediator = new MessageMediator(instances.size(),
				editorGUI, strategy);

		if (Constants.runTest)
			mediator.commandReader();

	}
}
