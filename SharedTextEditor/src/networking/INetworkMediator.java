package networking;

import message_types.Message;

public interface INetworkMediator {

	public void sendMessage(Message msg);
}
