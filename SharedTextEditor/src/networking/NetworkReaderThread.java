package networking;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import communication.IAlgorithm;

public class NetworkReaderThread extends Thread {
	private Socket readerSocket;
	private ObjectInputStream objInputStream;
	private IAlgorithm strategy;

	public NetworkReaderThread(Socket readerSocket, IAlgorithm strategy) {
		this.setReaderSocket(readerSocket);
		this.strategy = strategy;

		try {
			objInputStream = new ObjectInputStream(
					readerSocket.getInputStream());
		} catch (IOException e) {
			System.out.println("Unable to create read stream for socket "
					+ readerSocket.getPort());
			e.printStackTrace();
		}

	}

	public void run() {
		Object msg;
		boolean stopRunning = false;
		/*
		 * read information from the connected instances and update the text
		 * according to the received information
		 */
		while (!stopRunning) {
			/* read received object */
			try {
				//msg = (Message) objInputStream.readObject();
				msg = objInputStream.readObject();
				strategy.recvFromNetwork(msg);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				//System.err.println("Socket closed");
				stopRunning = true;
			}
		}
	}

	public Socket getReaderSocket() {
		return readerSocket;
	}

	public void setReaderSocket(Socket readerSocket) {
		this.readerSocket = readerSocket;
	}

}
