package networking;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import utils.Constants;
import utils.Instance;

public class NetworkConnector extends Thread {
	private HashMap<Integer, NetworkWriterSocket> connectSockets;
	private ArrayList<Instance> textEditorInstances;
	private Instance serverInstance;

	public NetworkConnector(Instance serverInstance,
			ArrayList<Instance> textEditorInstances) {
		this.textEditorInstances = textEditorInstances;
		this.serverInstance = serverInstance;
		this.connectSockets = new HashMap<Integer, NetworkWriterSocket>();
	}

	public void run() {
		Socket clientSocket;
		ArrayList<String> existingConnections = new ArrayList<String>(textEditorInstances.size());
		existingConnections.add(serverInstance.getInstanceName());

		do {
			/* connect to the other text editor instances */
			for (int i = 0; i < this.textEditorInstances.size(); i++) {
				Instance instance = this.textEditorInstances.get(i);
				if (!existingConnections.contains(instance.getInstanceName())) {
					try {
						clientSocket = new Socket(instance.getHostname(),
								instance.getPort());

						existingConnections.add(instance.getInstanceName());

						System.out.println("[Network Connector] "
								+ serverInstance.getInstanceName()
								+ " Connected to port "
								+ clientSocket.getPort());

						NetworkWriterSocket wrSocket = new NetworkWriterSocket(
								clientSocket);
						connectSockets.put(i, wrSocket);

					} catch (UnknownHostException e) {
						System.err
								.println("The host is not running yet. Polling...");
						try {
							Thread.sleep(Constants.WAITING_TIME);
						} catch (InterruptedException e1) {
						}
					} catch (IOException e) {
					}
				}
			}
		} while (connectSockets.size() != textEditorInstances.size() - 1);
	}

	public HashMap<Integer, NetworkWriterSocket> getConnectSockets() {
		return connectSockets;
	}

	public void setConnectSockets(
			HashMap<Integer, NetworkWriterSocket> connectSockets) {
		this.connectSockets = connectSockets;
	}

}
