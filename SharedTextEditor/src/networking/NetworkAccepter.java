package networking;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import communication.IAlgorithm;

import utils.Instance;

public class NetworkAccepter extends Thread {
	private ArrayList<NetworkReaderThread> acceptedConnections;
	private int noAcceptConnections;
	private ServerSocket serverSocket;
	private Instance currentInstance;
	private IAlgorithm strategy;

	public NetworkAccepter(Instance serviceId, int noAcceptInstances, IAlgorithm strategy) {
		acceptedConnections = new ArrayList<NetworkReaderThread>();
		this.noAcceptConnections = noAcceptInstances;
		this.currentInstance = serviceId;
		this.strategy = strategy;
	}

	public void run() {
		Socket acceptInstance;

		/* instantiate network server for accepting connections */
		try {
			serverSocket = new ServerSocket(currentInstance.getPort());
		} catch (IOException e) {
			e.printStackTrace();
		}

		/*
		 * accept connections from other text editor instances and start threads
		 * for receiving information from other threads
		 */
		for (int i = 0; i < this.noAcceptConnections; ++i) {
			acceptInstance = null;

			try {
				acceptInstance = serverSocket.accept();
				System.out.println("[Server Socket] "
						+ currentInstance.getHostname()
						+ " [accept port] "
						+ acceptInstance.getPort());

				NetworkReaderThread clientThread = new NetworkReaderThread(
						acceptInstance, strategy);
				clientThread.start();
				this.acceptedConnections.add(clientThread);

			} catch (IOException e) {
				System.err.println("Error accepting a connection!");
				e.printStackTrace();
			}
		}

		/* close all threads after finishing execution */
		for (NetworkReaderThread readerTh : acceptedConnections) {
			try {
				readerTh.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		try {
			serverSocket.close();
		} catch (IOException e) {
			System.out
					.println("An error occured while trying to close server socket");
			e.printStackTrace();
		}

	}

}
