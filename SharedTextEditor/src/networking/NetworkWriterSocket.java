package networking;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class NetworkWriterSocket {
	private Socket socket;
	private ObjectOutputStream oos;

	public NetworkWriterSocket(Socket socket) {
		this.socket = socket;
		try {
			oos = new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			System.err.println("Failed to create an output stream for socket "
					+ this.socket.getPort());
			e.printStackTrace();
		}
	}

	/**
	 * Send an insert or delete operation to the other text editor instances
	 * 
	 * @param op
	 *            The operation to be performed and the associated information
	 */
	public synchronized void sendObject(Object msg) {
		try {
			oos.writeObject(msg);
		} catch (IOException e) {
			System.out.println("Error trying to send an object on port "
					+ socket.getPort());
			e.printStackTrace();
		}
	}

}
