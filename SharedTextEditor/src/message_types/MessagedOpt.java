package message_types;

import java.io.Serializable;
import java.util.ArrayList;

import utils.Operation;
import utils.Priority;

public class MessagedOpt extends Message implements Serializable,
		Comparable<MessagedOpt> {
	private static final long serialVersionUID = 8226437181756834118L;
	private ArrayList<Integer> vectorTime;
	private Priority priority;
	private Operation transformedOp;

	public MessagedOpt(Operation op, ArrayList<Integer> vectorTime) {
		super(op);

		this.vectorTime = new ArrayList<Integer>();
		this.vectorTime.addAll(vectorTime);

		this.priority = new Priority();
	}

	public ArrayList<Integer> getVectorTime() {
		return vectorTime;
	}

	public void setVectorTime(ArrayList<Integer> vectorTime) {
		this.vectorTime = vectorTime;
	}

	/**
	 * The priority of an operation is equal with the id of the instance if
	 * there are no operations preceding the current one on the same position.
	 * Otherwise, the priority is a concatenation between the maximum priority
	 * of an operation on the same position and the id of the current instance.
	 * 
	 * @param maxPredecessor
	 *            the maximum priority of an operation executed on the same
	 *            position as the current one
	 */
	public void buildPriority(ArrayList<Integer> maxPredecessor) {
		if (!maxPredecessor.isEmpty())
			this.priority.priorityArray.addAll(maxPredecessor);

		this.priority.priorityArray.add(this.processId);
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public Operation getTransformedOp() {
		return transformedOp;
	}

	public void setTransformedOp(Operation transformedOp) {
		this.transformedOp = transformedOp;
	}

	@Override
	public int compareTo(MessagedOpt o) {
		ArrayList<Integer> recvVectorTime = o.getVectorTime();

		for (int i = 0; i < recvVectorTime.size(); i++) {
			if (this.vectorTime.get(i) > recvVectorTime.get(i))
				return 1;
			if (this.vectorTime.get(i) < recvVectorTime.get(i))
				return -1;
		}

		return 0;
	}
	
	public String toString() {
		return "[VT]: " + vectorTime;
	}

}
