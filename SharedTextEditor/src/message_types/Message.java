package message_types;

import java.io.Serializable;

import utils.Constants;
import utils.Operation;

public class Message implements Serializable{

	private static final long serialVersionUID = -8710431797586442769L;
	protected Operation op;
	protected Integer processId;

	public Message(Operation op) {
		this.setOp(op);
		this.setProcessId(Constants.processId);
	}

	public Operation getOp() {
		return op;
	}

	public void setOp(Operation op) {
		this.op = op;
	}

	public Integer getProcessId() {
		return processId;
	}

	public void setProcessId(Integer processId) {
		this.processId = processId;
	}

	@Override
	public String toString() {
		return "[ProcessID] " + processId + " ";
	}
}
