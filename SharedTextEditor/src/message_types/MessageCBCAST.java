package message_types;

import java.io.Serializable;
import java.util.ArrayList;

import utils.Operation;

public class MessageCBCAST extends Message implements Comparable<MessageCBCAST>, Serializable{

	private static final long serialVersionUID = 5486708144451762586L;
	private ArrayList<Integer> vectorTime;
	protected ArrayList<MessageCBCAST> deliverMsg;
	protected ArrayList<MessageCBCAST> delayedMsg;

	public MessageCBCAST(Operation op, ArrayList<Integer> vectorTime) {
		super(op);
		this.vectorTime = new ArrayList<Integer>();
		if (vectorTime != null)
			this.vectorTime.addAll(vectorTime);
		
	}

	
	public ArrayList<Integer> getVectorTime() {
		return vectorTime;
	}

	public void setVectorTime(ArrayList<Integer> vectorTime) {
		this.vectorTime.clear();
		this.vectorTime.addAll(vectorTime);
	}
	
	@Override
	public String toString() {
		return super.toString() + " [VT] " + vectorTime;
	}
	
	@Override
	public int compareTo(MessageCBCAST arg0) {
		boolean areEqual = true;
		ArrayList<Integer> otherVectorTime = arg0.getVectorTime();
		
		for (int i = 0; i < vectorTime.size(); i++) {
			if (vectorTime.get(i) > otherVectorTime.get(i))
				return 1;
			if (vectorTime.get(i) < otherVectorTime.get(i))
				areEqual = false;
		}
		
		if (areEqual)
			return 0;
		
		return -1;
	}
}
