package message_types;

import java.io.Serializable;
import java.util.Random;

import utils.Operation;

public class Message3Phase extends Message implements Serializable, Comparable<Message3Phase> {
	private static final long serialVersionUID = 537301102696486035L;
	private int timestamp;
	private Boolean deliverable;
	private Integer tag;
	private String phase;

	public Message3Phase(Operation op, Integer timestamp) {
		super(op);
		this.setTimestamp(timestamp);
		setDeliverable(Boolean.FALSE);
		
		/* generate message tag */
		Random rand = new Random();
		setTag(rand.nextInt());
	}

	public int getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public Integer getTag() {
		return tag;
	}

	public void setTag(Integer tag) {
		this.tag = tag;
	}

	public Boolean isDeliverable() {
		return deliverable;
	}

	public void setDeliverable(Boolean deliverable) {
		this.deliverable = deliverable;
	}

	@Override
	public String toString() {
		return super.toString() + " [tag] " + this.getTag() + " [phase] " + this.getPhase() + " [Timestamp] " + this.getTimestamp() + " [deliv] " + this.deliverable;
	}
	
	@Override
	public int compareTo(Message3Phase o) {
		int cmp = this.timestamp - o.getTimestamp();
//		if (cmp == 0)
//			return this.getProcessId() - o.getProcessId();
		return cmp;
	}

}
