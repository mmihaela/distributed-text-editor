package message_types;

import java.io.Serializable;

import utils.MessageStatus;
import utils.Operation;

public class MessageJupiter extends Message implements Serializable {

	private static final long serialVersionUID = -896339176996248618L;

	private MessageStatus msgStatus;

	public MessageJupiter(Operation op, MessageStatus msgStatus) {
		super(op);

		this.msgStatus = new MessageStatus();
		this.msgStatus.setNoGeneratedMsg(msgStatus.getNoGeneratedMsg());
		this.msgStatus.setNoRecvMsg(msgStatus.getNoRecvMsg());
	}

	public MessageStatus getMsgStatus() {
		return msgStatus;
	}

	public void setMsgStatus(MessageStatus msgStatus) {
		this.msgStatus = msgStatus;
	}

	public String toString() {
		return "[op]: " + this.op + msgStatus;
	}
}
