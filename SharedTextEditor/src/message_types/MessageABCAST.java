package message_types;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import utils.AbcastStatus;
import utils.Operation;

public class MessageABCAST extends MessageCBCAST implements Serializable{
	private static final long serialVersionUID = 6927468129092917230L;

	private String messageType;

	private Long uid;
	private ArrayList<AbcastStatus> deliveryOrder;

	public MessageABCAST(Operation op, ArrayList<Integer> vectorTime) {
		super(op, vectorTime);
		deliveryOrder = new ArrayList<AbcastStatus>();
		
		Random rand = new Random();
		this.uid = rand.nextLong();
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public ArrayList<AbcastStatus> getDeliveryOrder() {
		return deliveryOrder;
	}

	public void setDeliveryOrder(ArrayList<AbcastStatus> deliveryOrder) {
		this.deliveryOrder.addAll(deliveryOrder);
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		
		if (other == this)
			return true;
		
		if (! (other instanceof MessageABCAST))
			return false;
		
		MessageABCAST msg = (MessageABCAST)other;
		if (this.getUid() == msg.getUid())
			return true;
		
		return false;
	}
	
}
