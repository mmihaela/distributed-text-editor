package communication;

import java.util.HashMap;

import utils.Operation;
import networking.NetworkWriterSocket;

public interface IAlgorithm {

	public void internalMessage(Operation operation);
	public void recvFromNetwork(Object msg);
	public void setConnectedClients(HashMap<Integer, NetworkWriterSocket> sendSockets);
}
