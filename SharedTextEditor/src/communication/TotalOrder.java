package communication;

import gui.IGUI;

import java.util.HashMap;
import java.util.Map;

import message_types.Message;
import networking.NetworkWriterSocket;
import utils.Constants;
import utils.Operation;

public class TotalOrder implements IAlgorithm {
	private IGUI gui;
	protected HashMap<Integer, NetworkWriterSocket> sendSockets;

	public TotalOrder(IGUI gui) {
		this.gui = gui;
		sendSockets = new HashMap<Integer, NetworkWriterSocket>();
	}

	/**
	 * The client number 1 is considered the server socket and is the one that
	 * centralized the messages and send them back to the other clients. If the
	 * current node is the root node also deliver the message to the GUI
	 */
	@Override
	public void internalMessage(Operation operation) {
		/* root node sends message to all other instances */
		switch (Constants.processId) {
		case Constants.ROOT_NODE:
			synchronized (gui) {
				gui.multiplexMessage(operation);
			}

			/* send message to other instances */
			sendToGroup(operation);
			break;
		default:
			/* non-root instance sends message to root node */
			Message msg = createTotalOrderMsg(operation);
			synchronized (sendSockets) {
				NetworkWriterSocket rootSocket = sendSockets
						.get(Constants.ROOT_NODE);
				rootSocket.sendObject(msg);
			}
		}
	}

	@Override
	public synchronized void recvFromNetwork(Object obj) {
		Message msg = (Message) obj;
		synchronized (gui) {
			gui.multiplexMessage(msg.getOp());
		}

		if (Constants.processId == Constants.ROOT_NODE)
			sendToGroup(msg.getOp());

	}

	@Override
	public void setConnectedClients(
			HashMap<Integer, NetworkWriterSocket> sendSockets) {
		this.sendSockets.putAll(sendSockets);
	}

	private Message createTotalOrderMsg(Operation op) {
		Operation op_new = new Operation(op.getOpName(), op.getPosition(),
				op.getCharacter());

		return new Message(op_new);
	}

	/**
	 * Send message to all the nodes connected to the root node
	 * 
	 * @param op
	 *            the operation to be received by all the nodes
	 */
	private synchronized void sendToGroup(Operation operation) {
		Message msg = createTotalOrderMsg(operation);

		synchronized (sendSockets) {
			for (Map.Entry<Integer, NetworkWriterSocket> entry : sendSockets
					.entrySet()) {
				NetworkWriterSocket socket = entry.getValue();
				socket.sendObject(msg);
			}
		}

	}

}
