package communication;

import gui.IGUI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;

import message_types.MessageABCAST;
import networking.NetworkWriterSocket;
import utils.AbcastStatus;
import utils.Constants;
import utils.Operation;

public class ABCAST extends VectorTime {
	protected PriorityQueue<MessageABCAST> deliverQueue;
	protected PriorityQueue<MessageABCAST> delayedQueue;
	private ArrayList<MessageABCAST> setOrderMsgs;
	public static Comparator<MessageABCAST> vtComparator = new Comparator<MessageABCAST>() {

		@Override
		public int compare(MessageABCAST o1, MessageABCAST o2) {
			boolean areEqual = true;
			int ret = 0;
			ArrayList<Integer> o1VT = o1.getVectorTime();
			ArrayList<Integer> o2VT = o2.getVectorTime();

			for (int i = 0; i < o1VT.size(); i++) {
				if (o1VT.get(i) > o2VT.get(i))
					ret = 1;
				else if (o1VT.get(i) < o2VT.get(i))
					areEqual = false;
			}

			if (ret == 1)
				return Long.compare(o1.getUid(), o2.getUid());
			if (areEqual)
				return 0;

			return -1;
		}

	};

	public ABCAST(Integer size, IGUI gui) {
		super(size, gui);

		deliverQueue = new PriorityQueue<MessageABCAST>(20, vtComparator);
		delayedQueue = new PriorityQueue<MessageABCAST>(20, vtComparator);

		setOrderMsgs = new ArrayList<MessageABCAST>();
	}

	private boolean isDelayedMsg(ArrayList<Integer> senderTimes, int sender) {
		/* validate CBCAST conditions */
		synchronized (vectorTimes) {

			for (int i = 0; i < vectorTimes.size(); i++) {
				if ((i == sender) && (sender != Constants.processId)
						&& (vectorTimes.get(i) + 1 != senderTimes.get(i)))
					return true;

				if ((i != sender) && (senderTimes.get(i) > vectorTimes.get(i)))
					return true;
			}
		}

		System.out.println("[ " + Constants.processId + " ]"
				+ " message not delayed ");
		return false;
	}

	private void sendToAllNodes(MessageABCAST msg) {
		/* send message to all the nodes */
		synchronized (sendSockets) {
			for (Map.Entry<Integer, NetworkWriterSocket> entry : sendSockets
					.entrySet()) {
				NetworkWriterSocket socket = entry.getValue();
				socket.sendObject(msg);
			}
		}
	}

	protected MessageABCAST createMessage(Operation op) {

		Operation op_new = new Operation(op.getOpName(), op.getPosition(),
				op.getCharacter());

		/* increment message time before sending */
		incVectorTime();

		MessageABCAST msg;
		synchronized (vectorTimes) {
			msg = new MessageABCAST(op_new, this.vectorTimes);
		}

		return msg;
	}

	/**
	 * Received message from interface and sends it to all the other members of
	 * the group
	 */

	private void tokenHolderReceiver(MessageABCAST msg) {
		ArrayList<AbcastStatus> status = new ArrayList<AbcastStatus>();
		AbcastStatus statusMsg;

		synchronized (delayedQueue) {
			delayedQueue.add(msg);

			/*
			 * validate which delayed messages can get from delayed queue can be
			 * delivered
			 */
			Iterator<MessageABCAST> it = delayedQueue.iterator();
			while (it.hasNext()) {
				MessageABCAST delayed = it.next();
				if (!isDelayedMsg(delayed.getVectorTime(),
						delayed.getProcessId())) {
					statusMsg = new AbcastStatus(delayed.getUid(),
							delayed.getVectorTime());
					status.add(statusMsg);
				}
			}

			System.out.println("[status size] " + status.size());
			/* deliver messaged and update vector time */
			Collections.sort(status);

			System.out.println("Delayed queue before " + delayedQueue.size());
			for (int i = 0; i < status.size(); i++) {
				it = delayedQueue.iterator();
				while (it.hasNext()) {
					MessageABCAST delayed = it.next();
					if (delayed.getUid().equals(status.get(i).uid)) {
						it.remove();
						gui.multiplexMessage(delayed.getOp());
						updateVectorTime(delayed);
					}
				}
			}
			System.out.println("Delayed queue after " + delayedQueue.size());
			System.out.println(delayedQueue);
		}

		if (status.isEmpty())
			return;

		/* update vector time before sending */
		incVectorTime();

		MessageABCAST msgSetOrder;
		synchronized (vectorTimes) {
			msgSetOrder = new MessageABCAST(null, this.vectorTimes);
		}

		/* update info for set order message */
		msgSetOrder.setDeliveryOrder(status);
		msgSetOrder.setMessageType(Constants.SET_ORDER);

		System.out
				.println("[delivery order] " + msgSetOrder.getDeliveryOrder());
		System.out.println("========================================");
		/* send an set order msg */
		sendToAllNodes(msgSetOrder);
	}

	protected void updateVectorTime(MessageABCAST msg) {
		ArrayList<Integer> recvVectorTimes = msg.getVectorTime();

		synchronized (vectorTimes) {
			for (int i = 0; i < recvVectorTimes.size(); i++) {
				if (recvVectorTimes.get(i) > vectorTimes.get(i))
					vectorTimes.set(i, recvVectorTimes.get(i));
			}
		}
	}

	private void incVectorTime() {
		synchronized (vectorTimes) {
			/* update vector time */
			int newTime = this.vectorTimes.get(Constants.processId) + 1;
			this.vectorTimes.set(Constants.processId, newTime);
		}
	}

	private void applySetOrder(MessageABCAST setOrderMsg) {
		ArrayList<AbcastStatus> statusMsgs = setOrderMsg.getDeliveryOrder();
		ArrayList<MessageABCAST> toDeliverMsgs = new ArrayList<MessageABCAST>();

		synchronized (delayedQueue) {
			Iterator<MessageABCAST> itDelayed = delayedQueue.iterator();

			MessageABCAST cbcastMsg;

			/*
			 * verify if all the messages from a sets-order message exist in the
			 * delayed queue
			 */
			for (int i = 0; i < statusMsgs.size(); i++) {
				itDelayed = delayedQueue.iterator();
				while (itDelayed.hasNext()) {
					cbcastMsg = itDelayed.next();

					if (cbcastMsg.getUid().equals(statusMsgs.get(i).uid))
						toDeliverMsgs.add(cbcastMsg);
					else
						continue;
				}
			}

			System.out.println("To deliver size " + toDeliverMsgs.size());
			System.out.println("Status size " + statusMsgs.size());
			System.out.println(statusMsgs);

			if (toDeliverMsgs.isEmpty()
					|| toDeliverMsgs.size() != statusMsgs.size())
				return;

			System.out.println("[to deliver size] " + toDeliverMsgs.size());
			System.out.println("[DELAYED]" + delayedQueue);
			/*
			 * all the messages from the set order message were found and are
			 * now delivered and deleted from the delayed queue
			 */
			for (int i = 0; i < toDeliverMsgs.size(); i++) {
				itDelayed = delayedQueue.iterator();

				while (itDelayed.hasNext()) {
					cbcastMsg = itDelayed.next();

					if (cbcastMsg.getUid()
							.equals(toDeliverMsgs.get(i).getUid())) {
						gui.multiplexMessage(cbcastMsg.getOp());
						updateVectorTime(cbcastMsg);
						itDelayed.remove();
					}
				}
			}

		}

		synchronized (setOrderMsg) {
			setOrderMsgs.remove(0);
		}

	}

	/**
	 * An ordinary client receives a set order message and verifies which set
	 * order message from the received ones can be delivered.
	 * 
	 * @param msg
	 *            The message received from the network layer
	 */
	private void commonReceiver(MessageABCAST msg) {
		if (msg.getMessageType().equals(Constants.SET_ORDER)) {
			synchronized (setOrderMsgs) {
				setOrderMsgs.add(msg);
			}
		} else
			synchronized (delayedQueue) {
				delayedQueue.add(msg);
			}
		if (!setOrderMsgs.isEmpty())
			handleSetOrder();
	}

	private void handleSetOrder() {

		MessageABCAST setOrderFirstMsg;
		synchronized (setOrderMsgs) {
			setOrderFirstMsg = setOrderMsgs.get(0);
		}

		/* verify if all the messages from the first set order exist */
		applySetOrder(setOrderFirstMsg);

		System.out.println("=====================================");

	}

	@Override
	public void internalMessage(Operation operation) {
		MessageABCAST msg = createMessage(operation);

		msg.setMessageType(Constants.CBCAST);
		synchronized (delayedQueue) {
			delayedQueue.add(msg);
		}

		sendToAllNodes(msg);
	}

	@Override
	public synchronized void recvFromNetwork(Object msg) {
		MessageABCAST msg_abcast = (MessageABCAST) msg;

		switch (Constants.processId) {
		case Constants.TOKEN_HOLDER:
			tokenHolderReceiver(msg_abcast);
			break;
		default:
			commonReceiver(msg_abcast);
		}
	}

	@Override
	public void setConnectedClients(
			HashMap<Integer, NetworkWriterSocket> sendSockets) {
		this.sendSockets.putAll(sendSockets);
	}

}
