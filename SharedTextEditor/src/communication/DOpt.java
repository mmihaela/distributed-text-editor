package communication;

import gui.IGUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import message_types.MessagedOpt;
import networking.NetworkWriterSocket;
import utils.Constants;
import utils.Operation;
import utils.Priority;
import utils.TransformdOpt;

public class DOpt extends VectorTime {
	private ArrayList<MessagedOpt> logOp;
	private ArrayList<MessagedOpt> queuedOp;

	public DOpt(Integer size, IGUI gui) {
		super(size, gui);

		logOp = new ArrayList<MessagedOpt>();
		queuedOp = new ArrayList<MessagedOpt>();
	}

	@Override
	public void internalMessage(Operation operation) {
		if (Constants.runTest) {
			synchronized (gui) {
				gui.multiplexMessage(operation);
			}
		}

		MessagedOpt msg = createMessage(operation);

		/* add the message to the log queue */
		synchronized (logOp) {
			logOp.add(msg);
		}

		//updateVectorTime(msg);
		/* send the message to all the other entities from the network */
		broadcastMsg(msg);

	}

	@Override
	public synchronized void recvFromNetwork(Object obj) {
		MessagedOpt msg = (MessagedOpt) obj;
		MessagedOpt msgQueue;
		int compareResult;
		boolean hasDelivered = false;

		queuedOp.add(msg);

		/*
		 * go through the request queue and verify if there are messages that
		 * should be delivered
		 */
		do {
			hasDelivered = false;
			Iterator<MessagedOpt> it = queuedOp.iterator();
			while (it.hasNext()) {
				msgQueue = it.next();

				compareResult = compareVectorTime(this.vectorTimes,
												msgQueue.getVectorTime());
				
				switch (compareResult) {
				case -1:
					/*
					 * if vt(i) > vt(current site) don't execute, the element
					 * remains in the queue
					 */
					break;
				case 0:
					System.out.println("Equal times");
					/* deliver immediately the message */
					synchronized (gui) {
						gui.multiplexMessage(msgQueue.getOp());
					}

					updateVectorTime(msgQueue);
					it.remove();
					synchronized (logOp) {
						logOp.add(msgQueue);
					}
					hasDelivered = true;
					break;
				case 1:
					/*
					 * operation can be performed but a transformation has to be
					 * implemented
					 */
					transformOperation(msgQueue);
					System.out.println("Delivered " + msgQueue.getTransformedOp());
					if (msgQueue.getOp() != null) {
						synchronized (gui) {
							gui.multiplexMessage(msgQueue.getTransformedOp());
						}

						updateVectorTime(msgQueue);
						hasDelivered = true;
						it.remove();
						synchronized (logOp) {
							logOp.add(msgQueue);
						}
					}
					break;
				case 2:
					System.err.println("Inconsistent vector time");
					System.exit(1);
				}

			}
		} while (hasDelivered);
		System.out.println("=====================");
		System.out.println(queuedOp);
		System.out.println("=====================");
	}

	@Override
	public void setConnectedClients(
			HashMap<Integer, NetworkWriterSocket> sendSockets) {
		this.sendSockets.putAll(sendSockets);
	}

	@Override
	protected MessagedOpt createMessage(Operation op) {

		Operation op_new = new Operation(op.getOpName(), op.getPosition(),
				op.getCharacter());

		MessagedOpt msg = new MessagedOpt(op_new, this.vectorTimes);

		/* update priority of the message */
		msg.buildPriority(getMaxPredecessorPrio(op_new));

		return msg;
	}

	/**
	 * Find what is the maximum priority of a message that was already delivered
	 * on the same position as the new operation.
	 * 
	 * @param op
	 *            the operation to be transmitted to the other entities
	 */
	private ArrayList<Integer> getMaxPredecessorPrio(Operation op) {
		Priority maxPriority = new Priority();
		Integer positionOpLog;
		Priority priorityMsg;
		int refPos = op.getPosition();
		int prioSizesCompared;

		synchronized (logOp) {

			for (int i = 0; i < logOp.size(); i++) {
				positionOpLog = logOp.get(i).getOp().getPosition();

				if (!positionOpLog.equals(refPos))
					continue;

				priorityMsg = logOp.get(i).getPriority();

				if (maxPriority.isEmpty()) {
					maxPriority.updatePrio(priorityMsg);
					continue;
				}

				prioSizesCompared = maxPriority.comparePrioSize(priorityMsg);
				switch (prioSizesCompared) {
				case Constants.SMALLER:
					maxPriority.updatePrio(priorityMsg);
					break;
				case Constants.EQUAL:
					/*
					 * compare the lists element with element until the lists
					 * differ, then the list with the bigger number on that
					 * position is chosen
					 */
					maxPriority.updateWithMaxPrio(priorityMsg);
					break;
				case Constants.BIGGER:
					/*
					 * if the current maximum priority has a size bigger than
					 * the one of the analyzed message is considered it has
					 * priority
					 */
					break;

				}
			}
		}

		return maxPriority.priorityArray;
	}

	/**
	 * Apply a transformation on the operation to be delivered.
	 * 
	 * @param msg
	 *            the message to be transformed
	 */
	private void transformOperation(MessagedOpt msg) {
		MessagedOpt logMsg;

		synchronized (logOp) {
			for (int i = logOp.size() - 1; i >= 0; i--) {
				//logMsg = it.next();
				logMsg = logOp.get(i);

				/*
				 * if there are operations executed in the current site prior to
				 * the moment when the message from the remote site was
				 * generated
				 */
				int kVtElemLog = logMsg.getVectorTime().get(logMsg.getProcessId());
				int kVtElemQueue = msg.getVectorTime().get(logMsg.getProcessId());
				
				if (kVtElemQueue <= kVtElemLog) {
					String reqQueueOp = msg.getOp().getOpName();
					String logOp = logMsg.getOp().getOpName();

					/*
					 * apply transformation on the object according to
					 * operations types
					 */
					if (reqQueueOp.equals(Constants.INS_OP)
							&& logOp.equals(Constants.INS_OP))
						TransformdOpt.transformT11(msg, logMsg);

					else if (reqQueueOp.equals(Constants.INS_OP)
							&& logOp.equals(Constants.DEL_OP))
						TransformdOpt.transformT12(msg, logMsg);

					else if (reqQueueOp.equals(Constants.DEL_OP)
							&& logOp.equals(Constants.INS_OP))
						TransformdOpt.transformT21(msg, logMsg);

					else if (reqQueueOp.equals(Constants.DEL_OP)
							&& logOp.equals(Constants.DEL_OP))
						TransformdOpt.transformT22(msg, logMsg);
				}
			}
		}

	}

	private void broadcastMsg(MessagedOpt msg) {

		for (Map.Entry<Integer, NetworkWriterSocket> entry : sendSockets
				.entrySet()) {
			NetworkWriterSocket socket = entry.getValue();
			socket.sendObject(msg);
		}

	}

	protected void updateVectorTime(MessagedOpt msg) {
		int recvProc = msg.getProcessId();

		vectorTimes.set(recvProc, vectorTimes.get(recvProc) + 1);

		System.out.println("[VectorTime][after_print] " + this.vectorTimes);
	}
}