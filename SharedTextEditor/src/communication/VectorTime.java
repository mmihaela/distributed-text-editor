package communication;

import gui.IGUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import message_types.Message;
import message_types.MessageCBCAST;
import networking.NetworkWriterSocket;
import utils.Constants;
import utils.Operation;

public class VectorTime implements IAlgorithm {
	protected ArrayList<Integer> vectorTimes;
	protected HashMap<Integer, NetworkWriterSocket> sendSockets;

	protected IGUI gui;

	public VectorTime(Integer size, IGUI gui) {
		vectorTimes = new ArrayList<Integer>();
		this.gui = gui;

		for (int i = 0; i < size; i++)
			vectorTimes.add(0);

		sendSockets = new HashMap<Integer, NetworkWriterSocket>();
	}

	protected Message createMessage(Operation op) {

		/* update vector time */
		int newTime = this.vectorTimes.get(Constants.processId) + 1;

		Operation op_new = new Operation(op.getOpName(), op.getPosition(),
				op.getCharacter());

		/* increment message time before sending */
		this.vectorTimes.set(Constants.processId, newTime);
		System.out.println("[VectorTime][send] " + this.vectorTimes);

		MessageCBCAST msg = new MessageCBCAST(op_new, this.vectorTimes);

		return msg;
	}

	protected void updateVectorTime(MessageCBCAST msg) {
		ArrayList<Integer> recvVectorTimes = msg.getVectorTime();

		for (int i = 0; i < recvVectorTimes.size(); i++) {
			if (recvVectorTimes.get(i) > vectorTimes.get(i))
				vectorTimes.set(i, recvVectorTimes.get(i));
		}
		System.out.println("[VectorTime][recv] " + this.vectorTimes);
	}

	/**
	 * Compare the vector time of the current site with a given vector time
	 * 
	 * @param vt1
	 *            vector time of a message associated with other site
	 * @return
	 * 			-1 : vt(site) < vt(remote site);
	 * 			0 : vt(site) = vt(remote site);
	 * 			1 : vt(site) > vt(remote site);
	 */
	protected int compareVectorTime(ArrayList<Integer> vt1, ArrayList<Integer> vt2) {
		boolean retEqual = true;
		//boolean retGreater = true;
		boolean retSmaller = true;
		
		System.out.println("[vt1] " + vt1);
		System.out.println("[vt2] " + vt2);
		
		/* verify equality */
		for (int i = 0; i < vt1.size(); i++) {
			if (!vt1.get(i).equals(vt2.get(i))) {
				retEqual = false;
				break;
			}
		}
		
		if (retEqual)
			return 0;
		
		/* verify smaller than */
		for (int i = 0; i < vt1.size(); i++) {
			if (vt1.get(i) > vt2.get(i)) {
				retSmaller = false;
				break;
			}
		}
		if (retSmaller)
			return -1;

		return 1;

	}

	@Override
	public void internalMessage(Operation operation) {
		if (Constants.runTest)
			gui.multiplexMessage(operation);

		Message msg = createMessage(operation);

		for (Map.Entry<Integer, NetworkWriterSocket> entry : sendSockets
				.entrySet()) {
			NetworkWriterSocket socket = entry.getValue();
			socket.sendObject(msg);
		}
	}

	@Override
	public synchronized void recvFromNetwork(Object obj) {
		MessageCBCAST msg = (MessageCBCAST) obj;
		updateVectorTime(msg);
		gui.multiplexMessage(msg.getOp());

	}

	@Override
	public void setConnectedClients(
			HashMap<Integer, NetworkWriterSocket> sendSockets) {
		this.sendSockets.putAll(sendSockets);

	}

}
