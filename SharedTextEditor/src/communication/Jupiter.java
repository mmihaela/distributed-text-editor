package communication;

import gui.IGUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import message_types.MessageJupiter;
import networking.NetworkWriterSocket;
import utils.Constants;
import utils.MessageStatus;
import utils.Operation;
import utils.TransformJupiter;

public class Jupiter implements IAlgorithm {

	private MessageStatus myStatus;
	private Integer size;
	private ArrayList<MessageJupiter> outgoingMsgs;
	private HashMap<Integer, NetworkWriterSocket> sendSockets;
	private ArrayList<MessageStatus> msgStatus;

	protected IGUI gui;

	public Jupiter(Integer size, IGUI gui) {
		this.size = size;
		setMyStatus(new MessageStatus());

		outgoingMsgs = new ArrayList<MessageJupiter>();

		this.gui = gui;
		sendSockets = new HashMap<Integer, NetworkWriterSocket>();

		msgStatus = new ArrayList<MessageStatus>(this.size);
		for (int i = 0; i < this.size; i++) {
			msgStatus.add(i, new MessageStatus());
		}

		System.out.println("init jupiter");
	}

	@Override
	public void internalMessage(Operation operation) {
		if (Constants.runTest) {
			synchronized (gui) {
				gui.multiplexMessage(operation);
			}
		}

		/* send message */
		switch (Constants.processId) {
		case Constants.ROOT_NODE:
			System.out.println("Send broadcast");
			broadcastMessage(operation, false, null);
			break;
		default:
			System.out.println("Send to server");
			MessageJupiter msg = new MessageJupiter(operation, myStatus);
			sendToServer(msg);
			myStatus.incNoGeneratedMsg();
		}

	}

	@Override
	public void recvFromNetwork(Object msg) {
		MessageJupiter msgJupier = (MessageJupiter) msg;
		MessageJupiter queuedMsg;

		System.out.println("[NET]: " + msgJupier.getOp() + " [PID]: "
				+ msgJupier.getProcessId());

		/* discard acknowledged messages */
		Iterator<MessageJupiter> it = outgoingMsgs.iterator();
		while (it.hasNext()) {
			queuedMsg = it.next();

			if (queuedMsg.getMsgStatus().getNoGeneratedMsg() < msgJupier
					.getMsgStatus().getNoRecvMsg())
				it.remove();
		}

		/* transform the new message */
		for (int i = 0; i < outgoingMsgs.size(); i++) {
			xform(msgJupier, outgoingMsgs.get(i));
		}

		if (Constants.SERVER_NODE == Constants.processId) {
			broadcastMessage(msgJupier.getOp(), true, msgJupier.getProcessId());
		}

		/* deliver the transformed message */
		gui.multiplexMessage(msgJupier.getOp());

		System.out.println("===== Outgoing msg list ========");
		System.out.println(outgoingMsgs);
		System.out.println("====================");

		msgStatus.get(msgJupier.getProcessId()).incNoRecvMsg();

		myStatus.incNoRecvMsg();
	}

	@Override
	public void setConnectedClients(
			HashMap<Integer, NetworkWriterSocket> sendSockets) {
		this.sendSockets.putAll(sendSockets);
	}

	public ArrayList<MessageJupiter> getOutgoingMsgs() {
		return outgoingMsgs;
	}

	public void setOutgoingMsg(ArrayList<MessageJupiter> outgoingMsgs) {
		this.outgoingMsgs = outgoingMsgs;
	}

	/**
	 * As a server send message to all the clients when a message was generated
	 * internally by the server. If except parameter is set, the message is sent
	 * to all the clients except the one one mentioned through client parameter.
	 * 
	 * @param op
	 *            the message to be sent
	 */
	private void broadcastMessage(Operation op, boolean except, Integer client) {

		synchronized (sendSockets) {
			for (Map.Entry<Integer, NetworkWriterSocket> entry : sendSockets
					.entrySet()) {

				/* skip client when except parameter is set */
				if (except && entry.getKey().equals(client))
					continue;

				synchronized (msgStatus) {
					MessageJupiter msg = new MessageJupiter(op,
							msgStatus.get(entry.getKey()));

					NetworkWriterSocket socket = entry.getValue();
					socket.sendObject(msg);

					/* update number of messages sent to that destination */
					msgStatus.get(entry.getKey()).incNoGeneratedMsg();
				}
			}
		}

	}

	/**
	 * As a client send a message only to the clients
	 * 
	 * @param msg
	 *            the message to be sent
	 */
	private void sendToServer(MessageJupiter msg) {
		synchronized (sendSockets) {
			NetworkWriterSocket socketServer = sendSockets
					.get(Constants.SERVER_NODE);
			socketServer.sendObject(msg);
		}

	}

	/**
	 * Do transformation for the message received from the network but also for
	 * the message from the outgoing queue.
	 * 
	 * @param msg
	 *            message received from the network
	 * @param msgQueue
	 *            message from the outgoing message queue
	 */
	private void xform(MessageJupiter msg, MessageJupiter msgQueue) {
		Operation opRecvMsg = msg.getOp();
		Operation opQueueMsg = msgQueue.getOp();

		Operation opRecvTransformed = null;
		Operation opQueueTransformed = null;

		if (opRecvMsg.equals(Constants.INS_OP)
				&& opQueueMsg.equals(Constants.INS_OP)) {
			opRecvTransformed = TransformJupiter.transformT11(msg, msgQueue);
			opQueueTransformed = TransformJupiter.transformT11(msgQueue, msg);
		} else if (opRecvMsg.equals(Constants.INS_OP)
				&& opQueueMsg.equals(Constants.DEL_OP)) {
			opRecvTransformed = TransformJupiter.transformT12(msg, msgQueue);
			opQueueTransformed = TransformJupiter.transformT21(msgQueue, msg);
		} else if (opRecvMsg.equals(Constants.DEL_OP)
				&& opQueueMsg.equals(Constants.INS_OP)) {
			opRecvTransformed = TransformJupiter.transformT21(msg, msgQueue);
			opQueueTransformed = TransformJupiter.transformT12(msgQueue, msg);
		} else if (opRecvMsg.equals(Constants.DEL_OP)
				&& opQueueMsg.equals(Constants.DEL_OP)) {
			opRecvTransformed = TransformJupiter.transformT22(msg, msgQueue);
			opQueueTransformed = TransformJupiter.transformT22(msgQueue, msg);
		}

		msg.setOp(opRecvTransformed);
		msgQueue.setOp(opQueueTransformed);
	}

	public MessageStatus getMyStatus() {
		return myStatus;
	}

	public void setMyStatus(MessageStatus myStatus) {
		this.myStatus = myStatus;
	}

}
