package communication;

import gui.IGUI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import message_types.MessageCBCAST;

public class CBCAST extends VectorTime {
	protected ArrayList<MessageCBCAST> deliverMsg;
	protected ArrayList<MessageCBCAST> delayedMsg;

	public CBCAST(Integer size, IGUI gui) {
		super(size, gui);

		deliverMsg = new ArrayList<MessageCBCAST>();
		delayedMsg = new ArrayList<MessageCBCAST>();
	}

	private boolean isDelayedMsg(ArrayList<Integer> senderTimes, int sender) {
		/* validate CBCAST conditions */
		for (int i = 0; i < vectorTimes.size(); i++) {
			if ((i == sender) && (vectorTimes.get(i) + 1 != senderTimes.get(i)))
				return true;

			if ((i != sender) && (senderTimes.get(i) > vectorTimes.get(i)))
				return true;
		}

		System.out.println("Message not delayed ");
		return false;
	}

	@Override
	public synchronized void recvFromNetwork(Object obj) {
		MessageCBCAST msg = (MessageCBCAST) obj;
		Integer sender = msg.getProcessId();
		ArrayList<Integer> senderTimes = msg.getVectorTime();
		boolean toDelay = isDelayedMsg(senderTimes, sender);

		System.out.println("[sender] " + sender);
		System.out.println("[VT_proc] " + vectorTimes);
		System.out.println("[VT_recv] " + senderTimes);

		if (toDelay) {
			delayedMsg.add(msg);
		} else
			deliverMsg.add(msg);

		/* validate if there are messages that can be delivered */
		Iterator<MessageCBCAST> it = delayedMsg.iterator();

		while (it.hasNext()) {
			MessageCBCAST msgDelay = it.next();
			toDelay = isDelayedMsg(msgDelay.getVectorTime(),
					msgDelay.getProcessId());
			if (!toDelay) {
				it.remove();
				deliverMsg.add(msgDelay);
			}
		}

		Collections.sort(deliverMsg);

		for (MessageCBCAST toDeliver : deliverMsg) {
			gui.multiplexMessage(toDeliver.getOp());
			updateVectorTime(toDeliver);
		}

		deliverMsg.clear();
	}
}
