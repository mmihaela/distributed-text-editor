package communication;

import gui.IGUI;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;

import message_types.Message3Phase;
import networking.NetworkWriterSocket;
import utils.Constants;
import utils.Operation;
import utils.ProposedStatus;

public class ThreePhase implements IAlgorithm {
	private Integer clock;
	private int priority;
	private IGUI gui;
	private int size;
	protected HashMap<Integer, NetworkWriterSocket> sendSockets;
	private HashMap<Integer, ProposedStatus> proposedTimes;
	private PriorityQueue<Message3Phase> deliveryQueue;

	public ThreePhase(int size, IGUI gui) {
		this.size = size;
		this.gui = gui;
		this.clock = 0;
		this.priority = 0;
		
		deliveryQueue = new PriorityQueue<Message3Phase>();
		proposedTimes = new HashMap<Integer, ProposedStatus>();
		sendSockets = new HashMap<Integer, NetworkWriterSocket>();
	}

	@Override
	public void internalMessage(Operation operation) {
		Message3Phase msg;
		synchronized (this.clock) {
			this.clock++;
			msg =new Message3Phase(operation, this.clock);
		}

		/* mark message for "revise_ts" */
		msg.setPhase(Constants.REVISE_TS);

		/* for each message wait responses from each */
		synchronized (proposedTimes) {
			ProposedStatus status = new ProposedStatus();
			proposedTimes.put(msg.getTag(), status);
			System.out.println("[INTERN] " + msg);
		}

		sendToAllNodes(msg);
	}

	private void sendToAllNodes(Message3Phase msg) {
		/* send message to all the nodes */
		synchronized (sendSockets) {
			for (Map.Entry<Integer, NetworkWriterSocket> entry : sendSockets.entrySet()) {
				NetworkWriterSocket socket = entry.getValue();
				socket.sendObject(msg);
			}
		}
	}

	private void proposedTsMsg(Message3Phase msg) {
		synchronized (proposedTimes) {
			if (proposedTimes.isEmpty()) {
				System.out.println("[pid] " + Constants.processId + " !!! no msg pending !!!");
				return;
			}
			
			/* find pending message */
			ProposedStatus newstatus = proposedTimes.get(msg.getTag());
			if (newstatus == null)
				return;
			
			newstatus.no_nodes++;
			newstatus.timestamp = Math.max(newstatus.timestamp, msg.getTimestamp());

			System.out.println("[no_nodes] " + newstatus.no_nodes);
			
			/* send FINAL_TS message to all instances */
			if (newstatus.no_nodes == (size - 1)) {
				System.out.println("[RECV FROM ALL] " + msg.getTag());
				msg.setPhase(Constants.FINAL_TS);
				msg.setTimestamp(newstatus.timestamp);
				
				sendToAllNodes(msg);
				synchronized (this.clock) {
					clock = Math.max(this.clock, newstatus.timestamp);
				}

				
				/* display the character */
				if (Constants.runTest) {
					gui.multiplexMessage(msg.getOp());
				}

				proposedTimes.remove(msg.getTag());
			} else {
				System.out.println("[pid] " + Constants.processId + " [status] " + newstatus + " [tag] " + msg.getTag());
				/* update the existing values */
				proposedTimes.remove(msg.getTag());
				proposedTimes.put(msg.getTag(), newstatus);
			}
		}
	}

	private void reviseTsCase(Message3Phase msg) {
		int tmp = Math.max(this.priority + 1, msg.getTimestamp());
		this.priority = tmp;
		msg.setTimestamp(tmp);
		msg.setDeliverable(Boolean.FALSE);
		msg.setPhase(Constants.PROPOSED_TS);
		

		deliveryQueue.add(msg);
		System.out.println("------ REVISE TS -------");
		printPriorityQueue("");

		synchronized (sendSockets) {
			/* send proposed timestamp */
			NetworkWriterSocket socket = sendSockets.get(msg.getProcessId());
			System.out.println("[pid]" + Constants.processId + " [pid_send] " + msg.getProcessId());
			socket.sendObject(msg);
		}

	}
	
	private void printPriorityQueue(String when) {
		System.out.println("=============  " + when + "  ===============");
		for (Message3Phase msg_entry : deliveryQueue) {
			System.out.println(msg_entry);
		}
		System.out.println("============================");
	}

	private void finalTsCase(Message3Phase msg) {
		Message3Phase searchedMsg = null;
		Iterator<Message3Phase> it = deliveryQueue.iterator();
		while (it.hasNext()) {
			Message3Phase msg_entry = it.next();
			if (msg_entry.getTag().equals(msg.getTag())) {
				System.out.println("[pid] " + Constants.processId + " [final Ts] " + msg_entry.getTag());
				searchedMsg = msg_entry;
				//msg_entry.setTimestamp(msg.getTimestamp());
				//msg_entry.setDeliverable(Boolean.TRUE);
				it.remove();
				break;
			}
		}

		if (searchedMsg != null) {
			searchedMsg.setTimestamp(msg.getTimestamp());
			searchedMsg.setDeliverable(Boolean.TRUE);

			System.out.println("Timestamp recv " + msg.getTimestamp());
			deliveryQueue.add(searchedMsg);

			Integer firstMsgTag = deliveryQueue.peek().getTag();
			System.out.println("[pid]" + Constants.processId + "[tag first] " + firstMsgTag);
			printPriorityQueue("first_validate");
			//if (firstMsgTag.equals(searchedMsg.getTag())) {
				
				/* deliver all messages marked as deliverable from the set */
				it = deliveryQueue.iterator();
				while (it.hasNext()) {
					Message3Phase msg_entry = it.next();
					if (!msg_entry.isDeliverable())
						break;
					
					gui.multiplexMessage(msg_entry.getOp());
					
					synchronized (this.clock) {
						this.clock = Math.max(this.clock, msg_entry.getTimestamp()) + 1;
					}
					
					it.remove();
					printPriorityQueue("after_remove");
				}
			//}
		}

	}

	@Override
	public synchronized void recvFromNetwork(Object msg) {
		Message3Phase recv_msg = (Message3Phase) msg;

		/* verify the type of the message */
		switch (recv_msg.getPhase()) {
		case Constants.REVISE_TS:
			System.out.println("[pid] " + Constants.processId + "[REVISE TS] " + recv_msg.getTag());
			reviseTsCase(recv_msg);
			break;
		case Constants.PROPOSED_TS:
			System.out.println("[pid] " + Constants.processId + "[PROPOSED TS] " + recv_msg.getTag());
			proposedTsMsg(recv_msg);
			break;
		case Constants.FINAL_TS:
			System.out.println("[pid] " + Constants.processId + "[FINAL TS] " + recv_msg.getTag());
			finalTsCase(recv_msg);
			break;
		}

	}

	@Override
	public void setConnectedClients(
			HashMap<Integer, NetworkWriterSocket> sendSockets) {
		this.sendSockets.putAll(sendSockets);
	}

}
